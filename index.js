function validarRadio() {
    var radio =document.getElementsByClassName("inputRadio");
    
    for(i=0;i<radio.length;i++){
        if(radio[i].checked){
            return radio[i].value;
        }
    }
}

function guardarRegistro() {
    var txtDocumento = document.getElementById("documento");
    var txtNombre = document.getElementById("nombre");
    var txtPais = document.getElementById("pais");
    var txtCiudad = document.getElementById("ciudad");

    var documento = txtDocumento.value;
    var nombre = txtNombre.value;
    var pais = txtPais.value;
    var ciudad = txtCiudad.value;

    var objeto = {
        documento:documento,
        nombre:nombre,
        pais:pais,
        ciudad:ciudad
    };

    var storageType = validarRadio();

    if(storageType=="Local"){
        localStorage.setItem(documento, JSON.stringify(objeto));
        alert("Valor Ingresado en el LocalStorage.");
    } else if(storageType=="Session"){
        sessionStorage.setItem(documento, JSON.stringify(objeto));
        alert("Valor Ingresado en el SessionStorage.");
    } else {
        alert("Valor no Ingresado.");
    }
}

function leerRegistro(){
    var txtDocumento = document.getElementById("documento");

    var documento = txtDocumento.value;

    var storageType = validarRadio();
    
    if(storageType=="Local"){
        let datosUsuario = JSON.parse(localStorage.getItem(documento));
        
        alert("Datos del usuario en el LocalStorage: \n Documento: "+
        datosUsuario.documento+
        "\n Nombre: "+datosUsuario.nombre+
        "\n Pais: "+datosUsuario.pais+
        "\n Ciudad: "+datosUsuario.ciudad);
    } else if(storageType=="Session"){
        let datosUsuario = JSON.parse(sessionStorage.getItem(documento));
        
        alert("Datos del usuario en el SessionStorage: \n Documento: "+
        datosUsuario.documento+
        "\n Nombre: "+datosUsuario.nombre+
        "\n Pais: "+datosUsuario.pais+
        "\n Ciudad: "+datosUsuario.ciudad);
    } else {
        alert("No se han encontrado los datos");
    }
}

function borrarRegistro(){
    var txtDocumento = document.getElementById("documento");

    var documento = txtDocumento.value;

    var storageType = validarRadio();

    if(storageType=="Local"){
        localStorage.removeItem(documento);
        alert("Valor removido del LocalStorage.");
    } else if(storageType=="Session"){
        sessionStorage.removeItem(documento);
        alert("Valor removido del SessionStorage.");
    } else {
        alert("No se determino de cual storage se desea remover.");
    }
}

function limpiarRegistro(){
    var storageType = validarRadio();

    if(storageType=="Local"){
        localStorage.clear();
        alert("Se borro todo el LocalStorage.");
    } else if(storageType=="Session"){
        sessionStorage.clear();
        alert("Se borro todo el SessionStorage.");
    } else {
        alert("No se determino de cual storage se desea eliminar.");
    }
}